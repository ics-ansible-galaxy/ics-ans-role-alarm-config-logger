# ics-ans-role-alarm-config-logger

Ansible role to install alarm-config-logger.

## Role Variables

```yaml
alarm_config_logger_topics: []
alarm_config_logger_path: /opt/alarm-config-logger
alarm_config_logger_version: 4.6.1
alarm_config_logger_kafka_server: localhost:9092
alarm_config_logger_repo_location: /var/alarm-config-logger
alarm_config_logger_remote_repo_server: ""
alarm_config_logger_remote_repo_namespace: ""
alarm_config_logger_remote_repo_username: ""
alarm_config_logger_remote_repo_password: ""
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-alarm-config-logger
```

## License

BSD 2-clause
